# ImuInterface.ino #
is an arduino sketch that publishes Inertial Measurement Unit (IMU) sensor feedback through the topic imuData (*proc_imu/imuMsg*).

The IMU sensor used is [SparkFun 9 Degrees of Freedom - Sensor Stick](https://www.sparkfun.com/products/10724). The library used can be found [here](https://bitbucket.org/projectcosys/cosys-ros-pkg/src/4aed8a36fdc2bc45f710bb309caa7288eced782c/libraries/IMU9DOF/?at=arduino-devel) which is a slightly modified version of the [razor-9dof-ahrs](https://github.com/ptrbrtz/razor-9dof-ahrs/tree/master/Arduino/Razor_AHRS) library.