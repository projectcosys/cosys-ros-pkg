# proc_odom #
is a ROS package for conversion of raw encoder click to suitable message.

The message defined in this package:


1. odomMsg.msg
> Contains raw encoder clicks from encoders coupled with the left and the right wheel of the wheelchair system. The message definition is as below
```
#!ROS message
int16 lclicks
int16 rclicks
time t
```



The drivers (nodes) available in this package:


1. proc_odom_node.py 
> From the raw encoder clicks received from the topic topic encoder_click (*proc_odom/odomMsg*) compute and publish odom ([nav_msgs/Odometry](http://docs.ros.org/api/nav_msgs/html/msg/Odometry.html)).

