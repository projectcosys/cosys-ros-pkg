#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from turtle_actionlib.msg import Velocity
#indices of joystick buttons and axes
from JoyAxesButtons import angularIndex, linearIndex, move_base_enable

class JoyToTwist:
	def __init__(self):
		rospy.init_node('joy_to_twist', anonymous=True)
		rospy.Subscriber("joy", Joy, self.joyCallback)
		rospy.Subscriber("cmd_vel",Twist, self.navCallback)
		self.pub = rospy.Publisher("wheelchair/command_velocity",Velocity,queue_size=1)#rospy.Publisher("cmd_vel",Twist,queue_size=10)
		# spin() simply keeps python from exiting until this node is stopped
		self.joy_data = Joy()
		self.velocity = Velocity()
		#rospy.spin()

	def publish(self):
		self.pub.publish(self.velocity)

	def navCallback(self,cmd_vel):
		if self.joy_data.buttons[move_base_enable] == 1:
			self.velocity.linear = cmd_vel.linear.x
			self.velocity.angular = cmd_vel.angular.z
			    
	def joyCallback(self,joy):
		self.joy_data = joy
		if joy.buttons[move_base_enable]==0:
			cmd_vel = Twist()
			self.velocity.linear = joy.axes[linearIndex]
			self.velocity.angular = joy.axes[angularIndex]
					

		
if __name__ == "__main__":
        joyToTwist = JoyToTwist()
        rate = rospy.Rate(10) # 30hz
        while not rospy.is_shutdown():
                rospy.loginfo(rospy.get_caller_id() + "I heard %s %s\n",joyToTwist.velocity.linear,joyToTwist.velocity.angular);
                joyToTwist.publish()
                rate.sleep()



