#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from turtle_actionlib.msg import Velocity
#indices of joystick buttons and axes
from JoyAxesButtons import angularIndex, linearIndex, move_base_enable

class JoyToTwist:
	def __init__(self):
		rospy.init_node('joy_to_twist', anonymous=True)
		rospy.Subscriber("joy", Joy, self.joyCallback)
		self.pub = rospy.Publisher("wheelchair/command_velocity",Velocity,queue_size=10)
		self.velocity = Velocity()
		
	def publish(self):
		self.pub.publish(self.velocity)
	    
	def joyCallback(self,joy):
		self.velocity.linear = joy.axes[linearIndex]
		self.velocity.angular = joy.axes[angularIndex]
				
		
if __name__ == "__main__":
        joyToTwist = JoyToTwist()
        rate = rospy.Rate(10) # 10hz
        while not rospy.is_shutdown():
                rospy.loginfo(rospy.get_caller_id() + "I heard %s %s\n",joyToTwist.velocity.linear,joyToTwist.velocity.angular);
                joyToTwist.publish()
                rate.sleep()



