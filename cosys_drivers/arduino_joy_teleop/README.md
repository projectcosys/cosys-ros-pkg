# arduino_joy_teleop #
is a ROS package for teleoperation of collaborative wheelchair system (specifically the base and Pan-Tilt Unit - PTU).


The drivers (nodes) available in this package:


1. ArduinoTeleop.py 
> Maps the joystick input to base command velocity (for joystick attached to the wheelchair system)
2. ArduinoRemoteTeleop.py
> Maps the joystick input to base command velocity (for remote joystick - for remote teleoperation)
3. ArduinoTeleopPTU2.py
> Maps the joystick input to PTU command position (for joystick attached to the wheelchair system)
4. ArduinoRemoteTeleopPTU2.py
> Maps the joystick input to PTU command position (for remote joystick - for remote teleoperation)


Python scripts which aren't rosnodes:


1. JoyAxesButtons.py
> Contains indices of joystick axes and buttons
2. PTUCommands.py
> Contains limits of PTU command
