# proc_imu #
is a ROS package for conversion of raw imu data to suitable messages.

The message defined in this package:


1. imuMsg.msg
> Contains raw imu readings of all sensors viz. accelerometer, gyroscope, magnetometer. The message definition is as below
```
#!ROS message
int16[3] accel
int16[3] gyro
int16[3] magneto
```



The drivers (nodes) available in this package:


1. imuMsg2SensorImu.py 
> From the raw imu topic imu_data (*proc_imu/imuMsg*) compute imu/data_raw ([sensor_msgs/Imu](http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html))and imu/mag ([geometry_msgs/Vector3Stamped](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3Stamped.html)).
2. imuCalculateOffset.py
> Calculate average of raw imu data as received from the topic imu_data (*proc_imu/imuMsg*) for each sensors. 


Python scripts which aren't rosnodes:


1. imuOffsets.py
> Contains offset values of imu sensors (obtained from driver imuCalculateOffset.py of this package itself).
