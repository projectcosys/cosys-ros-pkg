# setup_tf_tree #
is a ROS package for generating tf tree of the wheelchair system based on wheel odometry (with and without EKF) and PTU feedback.


The drivers (nodes) available in this package:


1. setup_tf_tree_node
> cpp executable of source tf_broadcaster.cpp.