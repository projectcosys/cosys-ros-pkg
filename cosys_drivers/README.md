# cosys_drivers #
is a meta-package containing driver packages for collaborative wheelchair system.


The packages contained in this meta-package are:


1. arduino_joy_teleop
> For teleoperating wheelchair base and PTU.
2. proc_imu
> For converting the raw imu readings to the form recognizable by [imu_filter_madgwick](http://wiki.ros.org/imu_filter_madgwick) package.
3. proc_odom
> For converting raw encoder clicks to proper odometry message.
4. proc_ptu
> For converting raw feedback from PTU to physical orientation.
5. setup_tf_tree
> Generate a tf tree from wheelchair base odometry and PTU orientation.
