# parameters of the PTU and mapping of the feedback value to actual angle
#PTU parameters
TILT_ZERO_POSITION = 200
PAN_ZERO_POSITION = 512
#PTU feedback value limits
PTU_MIN_VALUE = 0 
PTU_MAX_VALUE = 1023
#PTU angle limits
PTU_MIN_ANGLE = 0
PTU_MAX_ANGLE = 300
