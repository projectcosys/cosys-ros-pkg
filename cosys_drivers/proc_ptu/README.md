# proc_ptu #
is a ROS package for conversion of raw PTU feedback to suitable message.

The message defined in this package:


1. ptuMsg.msg
> Contains raw PTU feedback. The message definition is as below
```
#!ROS message
int16 pan
int16 tilt
```



The drivers (nodes) available in this package:


1. generatePTUStamped.py 
> From the raw PTU feedback received from the topic ptu_msg (*proc_ptu/ptuMsg*) compute and publish ptu_stamped ([geometry_msgs/Vector3Stamped](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3Stamped.html)).
2. fakePTU.py
> Simulates a fake PTU that publishes the PTU feedback topic ptu_msg (*proc_ptu/ptuMsg*) based on the joystick topic joy ([sensor_msgs/Joy](http://docs.ros.org/api/sensor_msgs/html/msg/Joy.html)).



Python scripts which aren't rosnodes:


1. PTUParams.py
> Contains parameters of the PTU and mapping of the feedback value to actual angles.